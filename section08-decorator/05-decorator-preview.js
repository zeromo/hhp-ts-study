"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
{
    // https://fe-churi.tistory.com/41
    // 클래스 데코레이터는 클래스 선언 직전에 선언됩니다.
    // 클래스 데코레이터는 클래스 생성자에 적용되며 클래스 정의를 관찰, 수정 또는 교체하는 데 사용할 수 있습니다.
    function ClassDC(constructor) {
        console.log('Property decorator!');
        console.log(constructor);
    }
    // 메서드 데코레이터는 메서드 선언 직전에 선언됩니다.
    // 메서드 관찰, 수정 또는 대체하는 데 사용할 수 있습니다.
    function MethodDC(target, name, descriptor) {
        console.log('Method decorator!');
        console.log(target, name, descriptor);
    }
    // 접근자 데코레이터는 접근자 선언 바로 전에 선언됩니다.
    // 접근자 데코레이터는 접근자의 프로퍼티 설명자에 적용되며 접근자의 정의를 관찰, 수정 또는 교체하는 데 사용할 수 있습니다.
    function AccessorDC(target, name, descriptor) {
        console.log('Accessor decorator!');
        console.log(target, name, descriptor);
    }
    // 매개변수 데코레이터는 매개 변수 선언 직전에 선언됩니다.
    // 매개변수 데코레이터는 클래스 생성자 또는 메서드 선언의 함수에 적용됩니다.
    // 매개변수 데코레이터는 선언 파일, 오버로드 또는 다른 주변 컨텍스트에서 사용할 수 없습니다.
    function ParameterDC(target, name, position) {
        console.log('Parameter decorator!');
        console.log(target, name, position);
    }
    // 프로퍼티 데코레이터는 프로퍼티 선언 바로 전에 선언됩니다.
    // 프로퍼티 데코레이터는 선언 파일이나 다른 주변 컨텍스트에서 사용할 수 없습니다.
    function PropertyDC(target, propertyName) {
        console.log('Property decorator!');
        console.log(target, propertyName);
    }
    let Product = class Product {
        constructor(t, p) {
            this.title = t;
            this._price = p;
        }
        set price(val) {
            if (val > 0) {
                this._price = val;
            }
            else {
                throw Error('Invalid price = sould be positive!');
            }
        }
        getPriceWithTax(tax) { }
    };
    __decorate([
        PropertyDC
    ], Product.prototype, "title", void 0);
    __decorate([
        AccessorDC
    ], Product.prototype, "price", null);
    __decorate([
        MethodDC,
        __param(0, ParameterDC)
    ], Product.prototype, "getPriceWithTax", null);
    Product = __decorate([
        ClassDC
    ], Product);
}
