{

// https://fe-churi.tistory.com/41
// 클래스 데코레이터는 클래스 선언 직전에 선언됩니다.
// 클래스 데코레이터는 클래스 생성자에 적용되며 클래스 정의를 관찰, 수정 또는 교체하는 데 사용할 수 있습니다.
function ClassDC(constructor: Function) {
    console.log('Property decorator!');
    console.log(constructor);
}

// 메서드 데코레이터는 메서드 선언 직전에 선언됩니다.
// 메서드 관찰, 수정 또는 대체하는 데 사용할 수 있습니다.
function MethodDC(target: any, name: string | Symbol, descriptor: PropertyDescriptor) {
    console.log('Method decorator!');
    console.log(target, name, descriptor);
}

// 접근자 데코레이터는 접근자 선언 바로 전에 선언됩니다.
// 접근자 데코레이터는 접근자의 프로퍼티 설명자에 적용되며 접근자의 정의를 관찰, 수정 또는 교체하는 데 사용할 수 있습니다.
function AccessorDC(target: any, name: string, descriptor: PropertyDescriptor) {
    console.log('Accessor decorator!');
    console.log(target, name, descriptor);
}

// 매개변수 데코레이터는 매개 변수 선언 직전에 선언됩니다.
// 매개변수 데코레이터는 클래스 생성자 또는 메서드 선언의 함수에 적용됩니다.
// 매개변수 데코레이터는 선언 파일, 오버로드 또는 다른 주변 컨텍스트에서 사용할 수 없습니다.
function ParameterDC(target: any, name: string | Symbol, position: number) {
    console.log('Parameter decorator!');
    console.log(target, name, position);
}

// 프로퍼티 데코레이터는 프로퍼티 선언 바로 전에 선언됩니다.
// 프로퍼티 데코레이터는 선언 파일이나 다른 주변 컨텍스트에서 사용할 수 없습니다.
function PropertyDC(target: any, propertyName: string | Symbol) {
    console.log('Property decorator!');
    console.log(target, propertyName);
}


@ClassDC
class Product {
    @PropertyDC
    title: string;
    private _price: number;

    @AccessorDC
    set price(val: number) {
        if(val >0) {
            this._price = val;
        } else { 
            throw Error('Invalid price = sould be positive!');
        }
    }
    constructor(t: string, p: number) {
        this.title = t;
        this._price = p;
    }

    @MethodDC
    getPriceWithTax (@ParameterDC tax: Number) {}
}
}
