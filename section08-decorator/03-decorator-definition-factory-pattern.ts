{
// 클래스 등에 특정방법으로 적용되는 함수
// 무조건 1개 이상의 인자를 받아야 한다.
// 데코레이터의 실행은 클래스가 인스턴스화될 때가 아니라
// 정의될 때 싫생된다.
// 자바스크립트가 클래스 정의, 생성자 함수 정의를 찾으면 
// 데코레이터가 실행된다.
// @:decorator-name
// @:decorator-name() // if use factory-pattern
{
// 01. decorator definition - general
function Logger(constructor: Function) {
    console.log('Logging...');
    console.log(constructor);
}

// usage
@Logger
class Person {
    name = 'Max';
    constructor(){
        console.log('create person');
    }
}
const pers = new Person();
}
{
    
// 02. decoratro definition - use factory pattern
// 팩토리함수로 데코레이터 함수 실행 시
// 해당 함수가 사용하는 값을 바꿀 수 가 있다.
function Logger(logString: string) {
    return function(constructor: Function){
        console.log(logString);
        console.log(constructor);
    }
}

// usage
// Logger('LOGGING - PERSON')를 실행하여 
// 아래 decorator-function을 리턴
// function(constructor: Function){
//     console.log(logString);
//     console.log(constructor);
// }
@Logger('LOGGING - PERSON')
class Person {
    name = 'Max';
    constructor(){
        console.log('create person');
    }
}
const ppp = new Person();
}
}