"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
{
    // 클래스 등에 특정방법으로 적용되는 함수
    // 무조건 1개 이상의 인자를 받아야 한다.
    // 데코레이터의 실행은 클래스가 인스턴스화될 때가 아니라
    // 정의될 때 싫생된다.
    // 자바스크립트가 클래스 정의, 생성자 함수 정의를 찾으면 
    // 데코레이터가 실행된다.
    // @:decorator-name
    function withTemplate(template, hookId) {
        return function (constructor) {
            const hookEl = document.getElementById(hookId);
            const p = new constructor();
            if (hookEl) {
                hookEl.innerHTML = template;
                hookEl.querySelector('h1').textContent = p.name;
            }
        };
    }
    function Logger(constructor) {
        console.log('Logging...');
        console.log(constructor);
    }
    // 데코레이터는 여러개를 추가할 수 있다
    // 실행순서는 아래에서 위로 실행된다 bottom-up
    let Person = class Person {
        constructor() {
            this.name = 'Max';
            console.log('create person');
        }
    };
    Person = __decorate([
        Logger,
        withTemplate('<h1>My Person Object</h1>', 'app')
    ], Person);
    // const ppp = new Person();
}
