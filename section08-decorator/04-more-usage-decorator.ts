{
// 클래스 등에 특정방법으로 적용되는 함수
// 무조건 1개 이상의 인자를 받아야 한다.
// 데코레이터의 실행은 클래스가 인스턴스화될 때가 아니라
// 정의될 때 싫생된다.
// 자바스크립트가 클래스 정의, 생성자 함수 정의를 찾으면 
// 데코레이터가 실행된다.
// @:decorator-name
function withTemplate(template: string, hookId: string) {
    return function(constructor: any) {
        const hookEl = document.getElementById(hookId);
        const p = new constructor();
        if(hookEl) {
            hookEl.innerHTML = template;
            hookEl.querySelector('h1')!.textContent = p.name;
        }
    }
}

function Logger(constructor: Function) {
    console.log('Logging...');
    console.log(constructor);
}

// 데코레이터는 여러개를 추가할 수 있다
// 실행순서는 아래에서 위로 실행된다 bottom-up
@Logger
@withTemplate('<h1>My Person Object</h1>', 'app')
class Person {
    name = 'Max';
    constructor(){
        console.log('create person');
    }
}
// const ppp = new Person();
}