"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
{
    // 새로운 생성자를 반환하는 데코레이터
    // 정의되던 때 실행되던 로직을
    // 인스턴스 활 될때 실행되게 변경할 수 있다.
    function withTemplate(template, hookId) {
        return function (originalConstructor) {
            return class extends originalConstructor {
                constructor(..._) {
                    super();
                    const hookEl = document.getElementById(hookId);
                    if (hookEl) {
                        hookEl.innerHTML = template;
                        hookEl.querySelector('h1').textContent = this.name;
                    }
                }
            };
        };
    }
    function Logger(constructor) {
        console.log('Logging...');
        console.log(constructor);
    }
    // 데코레이터는 여러개를 추가할 수 있다
    // 실행순서는 아래에서 위로 실행된다 bottom-up
    let Person = class Person {
        constructor() {
            this.name = 'Max';
            console.log('create person');
        }
    };
    Person = __decorate([
        Logger,
        withTemplate('<h1>My Person Object</h1>', 'app')
    ], Person);
    // const ppp = new Person();
}
