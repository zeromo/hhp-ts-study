{
/**
 * optional-chaining
 * 객체에 어떤 속성이 정의되어있는지 모를때 사용
 */

type jobTP = {
    title: string,
    desc: string,
}

type UserTP = {
    id: string,
    name: string,
    job?: jobTP
}

interface UserIF {
    id: string, 
    name: string,
    job?: jobTP
}

// 01. use object literal
const fetchUserData: UserTP = {
    id: 'u1',
    name: 'asdf',
    // job: { title: 'ccc', desc: 'asdf'}
}

// console.log(fetchUserData.job.title);
// at js
console.log(fetchUserData.job && fetchUserData.job.title);
// as ts over v3.7
console.log(fetchUserData?.job?.title);

// 02. use class
class User implements UserIF {
    job?: jobTP
    constructor(public id: string, public name: string){}
}

const newUser = new User('aaa', 'bbb');
// at js
console.log(newUser.job && newUser.job.title);
// as ts over v3.7
console.log(newUser?.job?.title);


}