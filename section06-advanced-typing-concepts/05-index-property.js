"use strict";
{
    /**
     * index-property / index-type
     * -type-casting은 타입과 그 관리방법에 초점을 두진 않는다.
     * -타입과 그 관리방법에 초점을 두는 방식
     * -객체의 속성에 더큰 유연성을 부여해서 객체를 생성하는 특성
     *
     */
    // 01. 태그를 통해 접근할 경우 어떤 타입인지 정확히 안다.
    // COMMENT) const paragraph: HTMLParagraphElement | null
    const paragraph = document.querySelector('p');
    // 02. 아이디를 통해 접근할 경우 어떤 타입인지 정확히 모른다.
    // HTML파일을 분석하지 못해서 HTML요소라고만 여긴다.
    // COMMENT) const userInput: HTMLElement | null
    // const userInput = document.getElementById('user-input');
    // COMMENT) error: 'HTMLElement' 형식에 'value' 속성이 없습니다.
    // userInput.value = 'Hi there!';
    // >> 아래 두 방법으로 타입을 지정해준다.
    // const userInput = <HTMLInputElement> document.getElementById('user-input');
    const userInput = document.getElementById('user-input');
    userInput.value = 'Hi there!';
    const erorBag = {
        email: 'Not a valid email',
        username: 'Must start with a character',
    };
}
