{
/**
 * type-guard
 * 
 * using: typeof / in / instanceof / discriminated unions
 */
type Admin = {
    name: string;
    privileges: string[];
};

type Employee = {
    name: string;
    startDate: Date;
};

// interface ElevatedEmployee extends Admin, Employee {}
type ElevatedEmployee = Admin & Employee;

const el: ElevatedEmployee = {
    name: 'ppororo',
    privileges: ['create-server'],
    startDate: new Date(),    
}

type Combinable = string | number;
type Numeric = number | boolean;

type Universal = Combinable & Numeric;

// const universal: Universal = 'string';

// 01. typeof를 사용하는 type-guard
const add = function(a: Combinable, b: Combinable) {
    if(typeof a === 'string' || typeof b === 'string'){
        return a.toString() + b.toString();
    }

    return a + b;
}

// 02. in키워드를 사용하는 type-guard
type UnknownEmployee = Employee | Admin;
const printEmployeeInfo = function(emp: UnknownEmployee){
    console.log(emp.name);
    // not use
    // if(typeof emp === 'object') {}

    if('privileges' in emp){
        console.log(emp.privileges);
    }

    if('startDate' in emp){
        console.log(emp.startDate);
    }
}

// 03. instanceof를 사용하는 type-guard
class Car {
    drive(){
        console.log('Driving...');
    }
}
class Truck {
    drive(){
        console.log('Driving Truck...');
    }

    loadCargo(amount: number){}
}
type Vehicle = Car | Truck;
const v1 = new Car();
const v2 = new Truck();
const useVehicle = function(vehicle: Vehicle){
    vehicle.drive();

    // if('loadCargo' in vehicle){
    //     vehicle.loadCargo(1000);
    // }
    if(vehicle instanceof Truck){
        vehicle.loadCargo(1000);
    }
}

// 04. discriminated unions를 사용하는 type-guard
interface Bird {
    type: 'bird';
    flyingSpeed: number;    
}

interface Horse {
    type: 'horse'
    runningSpeed: number;
}

type Animal = Bird | Horse;

const moveAnimal = function(animal: Animal){
    let speed;

    switch (animal.type) {
        case 'bird':
            speed = animal.flyingSpeed;
            break;
        case 'horse':
            speed = animal.runningSpeed;
    }
           
    console.log(speed);
}
moveAnimal({type: 'bird', flyingSpeed: 1000});

}