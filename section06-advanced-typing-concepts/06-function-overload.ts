{
/**
 * function-overload
 * 하나의 함수에 여러 함수 시그니처를 정의하는 특성
 * 리턴타입이 무엇인지 정확히 알려줄 수 있다.
 * 
 * 예시에서 
 * 원본 함수는 리턴타입이 string이나 number를 가지게 되어
 * 함수호출시 어떤 타입이 리턴될지 알지 못하는데
 * 함수오버로딩을 통해 호출되는 파라미터의 타입에 따라
 * 각각의 리턴타입이 무엇인지 정의할 수 있어
 * TS가 함수호출시 어떤 리턴타입을 갖는지 정확히 알게 할 수 있다.
 * 
 */

type Combinable = string | number;

// 01. function-overload
// function add(a: number): number;
function add(a: number, b: number): number;
function add(a: string, b: string): string;
function add(a: number, b: string): string;
function add(a: string, b: number): string;
function add(a: Combinable, b: Combinable){
    if(typeof a === 'string' || typeof b === 'string'){
        return a.toString() + b.toString();
    }

    return a + b;
}

// 함수표현식으로 사용할 떄는.. 안되나?

const result1 = add(1, 5);
// const result2 = add('ppp', 'aaa') as string;
const result2 = add('ppp', 'aaa');

}