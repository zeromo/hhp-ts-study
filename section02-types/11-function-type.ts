{       
    /**
     * FUNCTION TYPE     * 
     * syntax) 
     *  GENERAL >> ': Function'
     *  let functionVar: Function; 
     * 
     *  DETAIL >> ': (param: paramType) => returnType'
     *  let functionVar: (param: string): void;
     * 
     */    
    //함수 선언문
    function DeclareAdd(n1: number, n2: number): number { return n1 + n2; }
    //함수 표현식
    let UnassignedExpressAdd = function(n1: number, n2: number){ return n1 + n2};
    // let ExpressAdd: Function = (n1: number, n2: number): number => n1 + n2; //using arrow syntax
    let GeneralExpressAdd: Function = function(n1: number, n2: number): number { return n1 + n2; };
    let DetailExpressAdd: (n1: number, n2: number) => number = function(n1: number, n2: number): number { return n1 + n2; };

    //01. 타입이 다른 변수에 할당
    let anyVar: number;
    anyVar = DeclareAdd; //error: '(n1: number, n2: number) => number' 형식은 'number' 형식에 할당할 수 없습니다.
    anyVar = UnassignedExpressAdd; //error: '(n1: number, n2: number) => number' 형식은 'number' 형식에 할당할 수 없습니다.
    anyVar = GeneralExpressAdd; //error: 'Function' 형식은 'number' 형식에 할당할 수 없습니다.
    anyVar = DetailExpressAdd;  //error: '(n1: number, n2: number) => number' 형식은 'number' 형식에 할당할 수 없습니다.

    //02. 타입이 Function인 변수에 할당
    //선언문, 표현식 둘다 가능
    let generalFuncVar: Function;
    generalFuncVar = DeclareAdd;
    generalFuncVar = UnassignedExpressAdd;
    generalFuncVar = GeneralExpressAdd;
    generalFuncVar = DetailExpressAdd;

    //03. 조금 더 구체적인 Function타입 설정
    let detailFuncVar: (a: number, b: number) => number;
    detailFuncVar = DeclareAdd;
    detailFuncVar = UnassignedExpressAdd;
    detailFuncVar = GeneralExpressAdd; //error: 'Function' 형식은 '(a: number, b: number) => number' 형식에 할당할 수 없습니다.
    detailFuncVar = DetailExpressAdd; 

    //04. function parameter is callback
}