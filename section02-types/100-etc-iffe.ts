{    
    // void를 찾으며 파도타기하다 정리하게된
    // 함수선언문, 함수표현식, 즉시실행함수, 모듈패턴
    
    // 1. 함수 선언문 Declaration
    function 함수명(){};

    // 2. 함수 표현식 Expression
    let 함수명 = function(){};
    
    // 3. 일반적인 즉시실행함수(Immediately-invoked Function Expression) 표현  / iffy로 발음  
    // IIFE를 구성하기 위해서는 함수 표현식이 필요하다. 함수 statement이나 정의는 IIFE를 만드는데 절대 이용될 수 없다.
    // 아래의 두가지 문체가 널리 사용되는데, 문체작동 방식이 약간다르다.
    (function iife(){})();
    (function iife(){}());
    
    // 4. 선언문을 식으로간주되게 하는 유용한 패턴
    !function iife(){}();
    void function iife(){}();

    // 5. 명확히 함수 표현식이라면 괄호를 사용하지 않아도 된다
    let result = function iife(){}();

    // 6. 클래식한 자바스크립트 모듈 패턴
    let Sequence = (function sequenceIIFE() {
        // 현재 counter 값을 저장하기 위한 Private 변수
        let current = 0;
        
        // IIFE에서 반환되는 객체
        return {
            getCurrentValue: function() {
            return current;  
            },
            
            getNextValue: function() {
            current = current + 1;
            return current;
            }
        };
    }());
}