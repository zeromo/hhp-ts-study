{       
    /**
     * TYPE ALIAS
     * 복잡한 type정의를 별칭으로 지정해두어 
     * 여러곳에서 재사용가능하거나 읽기 쉽게 만들어 준다.
     * syntax) type typeName = type;
     * type Combinable = number | string;
     * 
     */

    //01. not using type alias
    let combine1 = function(i1: number | string, i2: number | string, combineType: 'NUMBER' | 'STRING'){ /* LOGICAL CODE */ };
    
    //02. using type alias
    type Combinable = number | string;
    type CombineTypes = 'NUMBER' | 'STRING';
    let combine2 = function(i1: Combinable, i2: Combinable, combineType: CombineTypes){ /* LOGICAL CODE */ };
}