{       
    /**
     * FUNCTION RETURN TYPE
     * - 타입스크립트의 모든 type을 사용할 수 있다.
     * - can use any type in TypeScript.
     * 
     * syntax) function FuncName(param: parameterType): returnType {}
     * let haveReturnTypeFunc = function (param: string): string {};
     * 
     */
    let add = function(n1: number, n2: number): number{
        return n1 + n2;
    }
    let printResult = function(num: number): void{
        console.log(num);
    }
    printResult(add(1, 5));

    /**
     * 01. void
     * - '어떤것도 반환하지 않는다'는 의미.
     * - 관습적으로 return statement가 'return;'일 경우 return type으로 void를 사용
     * */    
    let doNotReturnAnythingFunc = function(num: number): void{
        //it have logical codes or not

        let undefinedValue: undefined;

        console.log(undefinedValue);
        // clearly do not return anything value
        // or returns a value of type undefined
        //  >> 'return;'
        //  >> 'return undefined;'
        //  >> 'return undefinedValue';
    }

    /**
     * 02. undefined
     * - undefiend는 ts에서 명확히 다룰 수 있는 type임.
     * - 따라서, '명확히 undefined를 반환한다'는 의미
     * - void와의 차이점: 
     * >> void는 리턴구문이 없어도 되지만 undefined는 반드시 undefined를 반환하는 리턴구문이 존재해야함.
     */
    let undefinedReturnFunc = function(): undefined{
        // it have logical codes or not

        let undefinedValue: undefined;
        // clearly returns a value of type undefined
        // >> return;
        // >> return undefined;
        // >> return undefinedValue;
        return;
    }
    /**
     * 03. unknown
     * - unknown: 어떤 값이 올지 모를 때 사용.
     * - ≒ any: 어떤 값도 올 수 있을 때 사용. 가장 폭넓게 쓰이며 type확인이 필요없게 함
     * - any보다 할 수 없는 것을 분명히 알려주고 type을 확인해볼 수 있는 장점이 있다.
     */    
    let anyInput: any;
    let unknownInput: unknown;
    let stringInput: string;

    // any는 어떤 값도 올수 있고, type확인이 필요없게 하기 때문에 에러가 발생하지 않습니다.
    stringInput = anyInput;
    // 아직 unkown의 타입이 무엇인지에 대한 타입지정을 해주지 않았기 때문에 에러가 발생합니다.
    stringInput = unknownInput; //error: 'unknown' 형식은 'string' 형식에 할당할 수 없습니다.

    // unknown에 대한 type checking코드를 삽입하면 에러를 없앨 수가 있습니다.
    // 이처럼 any보다 타입에 대하여 정확히 확인해야 하기 때문에 더욱 좋습니다.
    if(typeof unknownInput === 'string'){
        stringInput = unknownInput;
    }

    /**
     * 04. never
     * -void: 사실 진짜 아무것도 return하지 않는 것은 아닙니다.
     * -never: 절대 어떤것도 반환하지 않는다는 의미로 쓰이며 동작 합니다.
     * -never를 반환타입으로 지정하지 않아도 무방하지만 
     * 품질유지 관점에서, 반환타입으로 지정함으로서 의도에 대해 더 명확하게 정의할 수 있습니다.
     */
    // throw구문에서 오류가 발생하여 스크립트가 손상되기에 해당 함수는 정말 어떤 값도 생성하지 않습니다.
    function neverReturnFunc(message: string, code: number): never {
        throw { message, code };
    }
}