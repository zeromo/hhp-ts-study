{   
    /**
     * key point)
     *  1. types and how to use
     *  2. type alias
     */
    type AllTypes = string | number | boolean | {} | object | {'k1': any} | any[] | [any, any] | Enum | ('it' | 'that') | Function | ((param: any) => void) | any | unknown | undefined | never;

    // string
    let String: string = 'v1';
    type StringType = string;

    // number
    let Number: number = 0;
    type NumberType = numbe r;

    // boolean
    let Boolean: boolean = true;
    type BooleanType = boolean;

    // object
    let UnassignedObject = {'k1': 'v1', 'k2': 'v2'};    
    let AnyObject: any = {'k1': 'v1', 'k2': 'v2'};

    let GeneralObject: {} = {'k1': 'v1', 'k2': 'v2'};
    type GeneralObject = {};

    let GeneralObject2: object = {'k1': 'v1', 'k2': 'v2'};
    type GeneralObject2 = object;    

    let GeneralObject3: Object = {'k1': 'v1', 'k2': 'v2'};
    type GeneralObject3 = Object;

    let DetailObject: {'k1': string; 'k2': string;} = {'k1': 'v1', 'k2': 'v2'};
    type DetailObject = {'k1': string; 'k2': string;};    
    // object review: unassigned, any, general object, detail object타입 간 할당 시 동작
    // unassigned
    let unassignedVar;
    unassignedVar = UnassignedObject;
    unassignedVar = AnyObject;
    unassignedVar = GeneralObject;
    unassignedVar = GeneralObject2;
    unassignedVar = DetailObject;

    // any
    let anyVar: any;
    anyVar = UnassignedObject;
    anyVar = AnyObject;
    anyVar = GeneralObject;
    anyVar = GeneralObject2;
    anyVar = DetailObject;

    // general object({})
    let generalObjVar: {};
    generalObjVar = UnassignedObject;
    generalObjVar = AnyObject;
    generalObjVar = GeneralObject;
    generalObjVar = GeneralObject2;
    generalObjVar = DetailObject;

    // general object(object)
    let generalObjVar2: object;
    generalObjVar2 = UnassignedObject;
    generalObjVar2 = AnyObject;
    generalObjVar2 = GeneralObject;
    generalObjVar2 = GeneralObject2;
    generalObjVar2 = DetailObject;

    // detail object
    let detailObjVar: {k1: string; k2: string};
    detailObjVar = UnassignedObject;
    detailObjVar = AnyObject;
    detailObjVar = GeneralObject;  // error: 'k1' 속성이 '{}' 형식에 없지만 '{ k1: string; }' 형식에서 필수입니다.
    detailObjVar = GeneralObject2; // error: 'k1' 속성이 '{}' 형식에 없지만 '{ k1: string; }' 형식에서 필수입니다.
    detailObjVar = DetailObject;

    // array
    let Array: number[] = [1, 2, 3, 4];
    type ArrayType = any[];

    // tuple
    let Tuple: [string, number] = ['v1', 100];
    type TupleType = [any, any];

    // enum
    enum Enum { STRING, NUMBER };
    type EnumType = Enum; //재할당 가능    
    let _UsingEnum: Enum = Enum.STRING;
    let _UsingEnumType: EnumType = Enum.STRING;

    // JS CODE OF ENUM
    // before es6    
    var Enum = void 0;
    (function (Enum) {
        Enum[Enum["STRING"] = 0] = "STRING";
        Enum[Enum["NUMBER"] = 1] = "NUMBER";
    })(Enum || (Enum = {}));
    ;
    // since es6
    let Enum;
    (function (Enum) {
        Enum[Enum["STRING"] = 0] = "STRING";
        Enum[Enum["NUMBER"] = 1] = "NUMBER";
    })(Enum || (Enum = {}));
    ;

    // any
    let Any: any = 'any';
    type AnyType = any;
        let Any_Object: any = {'k1': 'any'};
        let Any_Array: any[] = [1, 'any', undefined, null];
        let Any_Tuple: [any, any] = [undefined, null];
        let Any_Union: any | any = null;
        let Any_Function: any = function(p1: any): void {};

    // union
    let Union: AllTypes = 'it is union';
    type UnionType = AllTypes; // 타입 간 재할당 가능

    // literal
    let Literal: 'onlyCanHaveFixedValue' = 'onlyCanHaveFixedValue';
    type LiteralType = 'onlyCanHaveFixedValue';

    // literalAndUnion
    let LiteralAndUnion: 'it' | 'that' = 'it';
    type LiteralAndUnionType = 'it' | 'that';

    // function
    // Assign Type To Function Param
    let FunctionAssignTypeToParam = function(
        _string: string,
        _number: number,
        _boolean: boolean,
        _object: {}, 
        _object2: object,
        _object3: {'k1': any},
        _array: any[],
        _tuple: [any, any],
        _enum: Enum,
        _any: any,
        _union: AllTypes,
        _literal: 'onlyCanHaveFixedValue',
        _literalAndUnion: 'it' | 'that',
        _function: Function,
        _function2: (param: any) => void,
        _void: void,
        _undefined: undefined,
        _never: never,
        _unknown: unknown){};
    // Assign Type to Function Return
    let FunctionAssignTypeToReturn = function(param: number): number { return param; }
    // Assign Function Type to Variable
    // 선언문(Decalarment)
    function UnassignedDeclareFunc(param: any): void{};
    // 표현식(Expression)
    let UnassignedExpressFunc = function(param: any): void{};
    let GeneralExpressFunc: Function = function(param: any): void{};
    type GeneralFunctionType = Function;
    let DetailExpressFunc: (param: any) => void = function(param: any): void{};
    type DetailFunctionType = (param: any) => void;
    // function review: unassigned(decalarment), unassigned(expression), general function(expression), detail function(expression) 타입 간 할당 시 동작    
    // unassigned
    unassignedVar = UnassignedDeclareFunc;
    unassignedVar = UnassignedExpressFunc;
    unassignedVar = GeneralExpressFunc;
    unassignedVar = DetailExpressFunc;

    // any
    anyVar = UnassignedDeclareFunc;
    anyVar = UnassignedExpressFunc;
    anyVar = GeneralExpressFunc;
    anyVar = DetailExpressFunc;

    // general Function
    let generalFuncVar: Function;
    generalFuncVar = UnassignedDeclareFunc;
    generalFuncVar = UnassignedExpressFunc;
    generalFuncVar = GeneralExpressFunc;
    generalFuncVar = DetailExpressFunc;

    //detail Function
    let detailFuncVar: (param: any) => void;
    detailFuncVar = UnassignedDeclareFunc;
    detailFuncVar = UnassignedExpressFunc;
    detailFuncVar = GeneralExpressFunc; // error: 'Function' 형식은 '(param: any) => void'
    detailFuncVar = DetailExpressFunc;

    // void
    let Void: void;
    type VoidType = void;

    // undefined
    let Undefined: undefined;
    type UndefinedType = undefined;

    // unkown
    let Unknown: unknown;
    type UnkownType = unknown;

    // never
    let Never: never;
    type NeverType = never;
}