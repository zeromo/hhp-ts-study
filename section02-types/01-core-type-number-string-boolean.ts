/**
 * 01. TYPESCRIPT CORE-TYPES
 * 1. number
 * -All numbers, no differentiation between
 * integers of floats
 * 
 * 2. string
 * -All text values
 * -using this...
 * --single-quotaion, ''
 * --double-quotation, ""
 * --backtick, ``
 * 
 * 3. boolean
 * -Just these two, no "trutyh" or "falsy" values
 * -true | false 
 */

let add = function(n1, n2){
    return n1 + n2;    
}

let number1 = 5;
let number2 = 2.8;
let result = add(number1, number2);
/**
 * result is 7.8 but, 
 * if number1 have string '5'
 * then result is 52.8 
 * because javscript convers number 2.8 to strig type
 * and concat '5' and '2.8'
 *  >> typescript can type assign in function parameters
 *  >> goto 02.
 * 
 * 이 기능은 코드를 자바스크립트로
 * 컴파일하기 전 개발중에만 도움이 될 수 있습니다.
 * 브라우저는 해당 기능을 가지고 있지 않습니다.
 * 런타임 코드는 변경되지 않습니다.
 * 이를 통해 단계와 온전성 검사를 추가합니다.
 * 
 */
console.log(result);

/**
 * 02. assign type to function parameters
 * parameter name: type
 */
add = function(n1: string, n2: number){
    /**
     * if n1 or n2 is not string type,
     * we can get error message
     * like 'typescript can type assign in function parameters'
     * during compiling.     * 
     */
    return n1 + n2;
}

/**
 * number1 = '5';
 */
number1 = 5;
number2 = 2.8;
console.log(add(number1, number2));

/**
 * 03. validation for typeof operator
 * -Javascript uses 'dynamic types'
 * (resolved at runtime).
 * -Typescript uses 'static types'
 * (set during development).
 */
add = function(n1: number, n2: number){
    /**
     * traditional validation     * 
     * 런타임중에 유형을 가져오는 것이 유용할 수도 있지만
     * 개발 중에 얻는 것이 훨씬 중요할 수 있다.
     * Typescript 기능과 검사가
     * 자바스크립트 엔진에 내장되어 있지 않기 때문에
     * runtime에서가 아니라 개발 중에만 지원받을 수 없다.
     * 코드를  compile할 때만 가능함.     * 
     */
    // if(typeof n1 !== 'number' || typeof n2 !== 'number'){
    //     throw new Error('Incorrect input');
    // }

    return n1 + n2;
}

console.log(add(5, 6));

/**
 * 04. type case
 * 타입스크립트에서는 항상 string 또는 number와 같은 타입을 다룹니다.
 * 중요:  string , number 등이 아니라 String 및 Number 등 입니다.
 * 타입스크립트의 주요 원시 타입은 모두 소문자입니다!
 */