"use strict";
{
    /**
     * LITERAL TYPE: core-type(number, string, boolean)만 가능하다.
     * value domain을 지정 할 때 사용한다. <> enum을 사용할 수도 있다.
     * 해당 특징에 의해 주로 union-type과 연동해서 사용된다.
     * operator: '|'
     * format: ': fixedValue';
     * ex)
     * let numberLiteral: 100 = 100;
     * let stringLiteral: 'onlyCanHaveThisValue' = 'onlyCanHaveThisValue';     *
     *
     * -- EACH LITERAL SYNTAX OF TYPES
     * let coreTypes: string | number | boolean = 'string';
     * let objTypes: { 'k1': string | number; } = { 'k1': 'v1' };
     * let fcTypes = function(p1: string | number){};
     *
     * -- CONVERT USING ENUM
     * enum Enum { A = 'string', B = 0 };
     * let coreTypes: Enum = Enum.A; //boolean은 열거형에서 사용못함
     * let objTypes: { 'k1': Enum; } = { 'k1': Enum.A };
     * let fcTypes = function(p1: Enum){ console.log(p1); };
     *
     */
    /**
     * 01. literal-type을 통한 value domain 지정
     * */
    // let combine = function(i1: string | number, i2: string | number, type: string){
    // let combine = function(i1: string | number, i2: string | number, asNumber: boolean){
    let combineUsingLiteral = function (i1, i2, combineType) {
        // i1과 i2의 타입을 유니온타입을 통해 지정하였지만 타입스크립트는
        // '+' 연산자가 사용되지 않는 타입이 올수도 판단하여 에러를 제공한다.
        // (=정확히 어떤 타입이 오는지는 runtime에서 확인할 수 있는 사항이기 때문에)
        // error message: '+' 연산자를 'string | number' 및 'string | number' 형식에 적용할 수 없습니다.
        // const result = i1 + i2; 
        // 어떤 로직을 처리하느냐에 따라 
        // 부가적인 런타임 타입체크(extra runtime type check) 여부가 갈림
        let result;
        if (typeof i1 === 'number' && typeof i2 === 'number' || combineType === 'NUMBER') {
            // result = Number(i1) + Number(i2);
            result = +i1 + +i2;
        }
        else {
            result = i1.toString().concat(i2.toString());
        }
        return result;
    };
    const numberCombineUsingLiteral = combineUsingLiteral(1, 2, 'NUMBER');
    const stringCombineUsingLiteral = combineUsingLiteral('A', 'B', 'STRING');
    /**
     * 02. enum-type을 통한 value domain 지정
     */
    let CombineType;
    (function (CombineType) {
        CombineType[CombineType["NUMBER"] = 0] = "NUMBER";
        CombineType[CombineType["STRING"] = 1] = "STRING";
    })(CombineType || (CombineType = {}));
    ;
    let combineUsingEnum = function (i1, i2, combineType) {
        let result;
        if (typeof i1 === 'number' && typeof i2 === 'number' || combineType === CombineType.NUMBER) {
            result = +i1 + +i2;
        }
        else {
            result = i1.toString().concat(i2.toString());
        }
        return result;
    };
    const numberCombineUsingEnum = combineUsingEnum(1, 2, CombineType.NUMBER);
    const stringCombineUsingEnum = combineUsingEnum('A', 'B', CombineType.STRING);
}
