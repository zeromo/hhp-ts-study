"use strict";
{
    // string
    let String = 'v1';
    // number
    let Number = 0;
    r;
    // boolean
    let Boolean = true;
    // object
    let UnassignedObject = { 'k1': 'v1', 'k2': 'v2' };
    let AnyObject = { 'k1': 'v1', 'k2': 'v2' };
    let GeneralObject = { 'k1': 'v1', 'k2': 'v2' };
    let GeneralObject2 = { 'k1': 'v1', 'k2': 'v2' };
    let GeneralObject3 = { 'k1': 'v1', 'k2': 'v2' };
    let DetailObject = { 'k1': 'v1', 'k2': 'v2' };
    // object review: unassigned, any, general object, detail object타입 간 할당 시 동작
    // unassigned
    let unassignedVar;
    unassignedVar = UnassignedObject;
    unassignedVar = AnyObject;
    unassignedVar = GeneralObject;
    unassignedVar = GeneralObject2;
    unassignedVar = DetailObject;
    // any
    let anyVar;
    anyVar = UnassignedObject;
    anyVar = AnyObject;
    anyVar = GeneralObject;
    anyVar = GeneralObject2;
    anyVar = DetailObject;
    // general object({})
    let generalObjVar;
    generalObjVar = UnassignedObject;
    generalObjVar = AnyObject;
    generalObjVar = GeneralObject;
    generalObjVar = GeneralObject2;
    generalObjVar = DetailObject;
    // general object(object)
    let generalObjVar2;
    generalObjVar2 = UnassignedObject;
    generalObjVar2 = AnyObject;
    generalObjVar2 = GeneralObject;
    generalObjVar2 = GeneralObject2;
    generalObjVar2 = DetailObject;
    // detail object
    let detailObjVar;
    detailObjVar = UnassignedObject;
    detailObjVar = AnyObject;
    detailObjVar = GeneralObject; // error: 'k1' 속성이 '{}' 형식에 없지만 '{ k1: string; }' 형식에서 필수입니다.
    detailObjVar = GeneralObject2; // error: 'k1' 속성이 '{}' 형식에 없지만 '{ k1: string; }' 형식에서 필수입니다.
    detailObjVar = DetailObject;
    // array
    let Array = [1, 2, 3, 4];
    // tuple
    let Tuple = ['v1', 100];
    // enum
    let Enum;
    (function (Enum) {
        Enum[Enum["STRING"] = 0] = "STRING";
        Enum[Enum["NUMBER"] = 1] = "NUMBER";
    })(Enum || (Enum = {}));
    ;
    let _UsingEnum = Enum.STRING;
    let _UsingEnumType = Enum.STRING;
    // JS CODE OF ENUM
    // before es6    
    var Enum = void 0;
    (function (Enum) {
        Enum[Enum["STRING"] = 0] = "STRING";
        Enum[Enum["NUMBER"] = 1] = "NUMBER";
    })(Enum || (Enum = {}));
    ;
    // since es6
    let Enum;
    (function (Enum) {
        Enum[Enum["STRING"] = 0] = "STRING";
        Enum[Enum["NUMBER"] = 1] = "NUMBER";
    })(Enum || (Enum = {}));
    ;
    // any
    let Any = 'any';
    let Any_Object = { 'k1': 'any' };
    let Any_Array = [1, 'any', undefined, null];
    let Any_Tuple = [undefined, null];
    let Any_Union = null;
    let Any_Function = function (p1) { };
    // union
    let Union = 'it is union';
    // literal
    let Literal = 'onlyCanHaveFixedValue';
    // literalAndUnion
    let LiteralAndUnion = 'it';
    // function
    // Assign Type To Function Param
    let FunctionAssignTypeToParam = function (_string, _number, _boolean, _object, _object2, _object3, _array, _tuple, _enum, _any, _union, _literal, _literalAndUnion, _function, _function2, _void, _undefined, _never, _unknown) { };
    // Assign Type to Function Return
    let FunctionAssignTypeToReturn = function (param) { return param; };
    // Assign Function Type to Variable
    // 선언문(Decalarment)
    function UnassignedDeclareFunc(param) { }
    ;
    // 표현식(Expression)
    let UnassignedExpressFunc = function (param) { };
    let GeneralExpressFunc = function (param) { };
    let DetailExpressFunc = function (param) { };
    // function review: unassigned(decalarment), unassigned(expression), general function(expression), detail function(expression) 타입 간 할당 시 동작    
    // unassigned
    unassignedVar = UnassignedDeclareFunc;
    unassignedVar = UnassignedExpressFunc;
    unassignedVar = GeneralExpressFunc;
    unassignedVar = DetailExpressFunc;
    // any
    anyVar = UnassignedDeclareFunc;
    anyVar = UnassignedExpressFunc;
    anyVar = GeneralExpressFunc;
    anyVar = DetailExpressFunc;
    // general Function
    let generalFuncVar;
    generalFuncVar = UnassignedDeclareFunc;
    generalFuncVar = UnassignedExpressFunc;
    generalFuncVar = GeneralExpressFunc;
    generalFuncVar = DetailExpressFunc;
    //detail Function
    let detailFuncVar;
    detailFuncVar = UnassignedDeclareFunc;
    detailFuncVar = UnassignedExpressFunc;
    detailFuncVar = GeneralExpressFunc; // error: 'Function' 형식은 '(param: any) => void'
    detailFuncVar = DetailExpressFunc;
    // void
    let Void;
    // undefined
    let Undefined;
    // unkown
    let Unknown;
    // never
    let Never;
}
