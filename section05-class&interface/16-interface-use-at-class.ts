{
/**
 * USE INTERFACE AT CLASSES
 * implements키워드를 통해 복수개의 인터페이스를 할당한다.
 * 
 * syntax)
 * class :class-name implements :interface1, :interface2 {
 *   :prop-name: string;
 *    
 *   constructor(){}
 *   :method-name(:prop: :prop-type): :return-type{
 *      //logic
 *   }
 * }
 */

interface Greetable {
    // error: 인터페이스 속성에는 이니셜라이저를 사용할 수 없습니다.
    // name: string = 'ppororo';
    name: string;
    age: number;

    greet(phrase: string): void;
}

class Person implements Greetable {
    name: string;
    age: number = 30;

    constructor(name: string){
        this.name = name;
    }

    greet(phrase: string): void {
        console.log(phrase + '' + this.name);
    }
}

// 변수의 타입에 할당한 인터페이스를 사용해도 문제없다.
const geet: Greetable = new Person('geet');
const max: Person = new Person('MAX');

}