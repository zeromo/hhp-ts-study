{
    /** INTERFACE / ONLY TS NOT JS
     * 인터페이스는 객체의 구조를 정의
     * 
     * interface :interface-name {
     * 
     * }
     */

    interface Person {
        //error: 인터페이스 속성에는 이니셜라이저를 사용할 수 없습니다.
        // name: string = 'ppororo';
        name: string;
        age: number;

        greet(phrase: string): void;
    }

    let user1: Person;
    user1 = { 
        name: 'ppororo',
        age: 999,
        greet(phrase: string){
            console.log(phrase);
        }
    };
    user1.greet('hello minnasang');
}