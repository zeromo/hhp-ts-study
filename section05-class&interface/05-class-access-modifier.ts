{

/**
 * 접근지정자 / only TS not JS
 * -public
 *   > public을 명시하지 않은 프로퍼티와 동일하다.
 *   > 클래스외부에서 접근이 가능하다.
 * -private
 *   > 클래스외부에서 접근이 불가능하다. * 
 * 
 */


class Department {
    // name: string = 'DEFAULT';
    // name: string;
    public name: string;
    // employees: string[] = [];
    //private인 employees는 클래스 내에서만 접근 할 수 있따.
    private employees: string[] = [];

    // 이 클래스 및 이 클래스에 기반하여 생성된 객체에
    // 연결된 함수로, 객체가 생성될 때 실행됨.
    // 객체의 초기화 작업을 수행
    constructor(n: string){
        this.name = n;
    }


    // TS에서는 this를 매개변수로 받아 타입을 지정해 줄 수 있다.
    // TS가 이것을 힌트로 파악하여 무엇을 참고해야 할지 알아낼 수 있다.
    //  *철저히 TS의 문법이므로 JS에서는 에러가 발생한다.
    // describe() {
    //     console.log('Department: ' + this.name);
    // }
    describe(this: Department) {
        console.log('Department: ' + this.name);
    }

    addEmployee(employee: string){
        this.employees.push(employee);
    }

    printEmployeeInformation(){
        console.log(this.employees.length);
        console.log(this.employees);
    }
}

const accounting = new Department('Accounting');
accounting.addEmployee('kim');
accounting.addEmployee('lee');

//class내부의 변수인 employees를 외부에서 변경할 수 있음.
//클래스를 이용하는 방식을 하나로 정해고 이외는 사용하지 않아야 한다.
//팀단위로 작업할 경우 팀원마다 다른방식으로 employee를 추가하게 되면 혼선이 빚어진다.
//따라서, 클래스 외부에서 employees에 접근할 수 없게 해야한다.
//priavate으로 선언된 eemployees는 에러가 발생한다.
accounting.employees[2] = 'anna'; //error: 'employees' 속성은 private이며 'Department' 클래스 내에서만 액세스할 수 있습니다.

accounting.describe();


// 01. Department클래스의 describe메소드에서 this를 매개변수로 받아 타입을 지정한 경우
// accountingCopy에 name속성이 없어서 에러처리된다.
// accountingCopy에 name속성을 추가하면 에러가 없어진다.
// const accountingCopy = {describe: accounting.describe};
const accountingCopy = {name: 'CopyAccounting', describe: accounting.describe};
accountingCopy.describe();

// 01. Department클래스의 describe메소드에서 this를 매개변수로 받아 타입을 지정하지 않은 경우
// 콘솔에 undefined가 찍힌다.
// accountingCopy.describe();
}