"use strict";
{
    /**
     * readonly property (=keyword) / only TS not JS
     * 특정필드가 초기화 이후에 값이 변경되면 안될때 사용합니다.
     * readonly가 선언된 클래스 프로퍼티는 선언 시 또는 생성자 내부에서만 값을 할당할 수 있다.
     * 그 외의 경우에는 값을 할당할 수 없고 오직 읽기만 가능한 상태가 된다.
     * 이를 이용하여 상수의 선언에 사용한다.
     *
     * syntax)
     *  01)
     *  class :class-name {
     *    :access-modifier readonly :variable-name: :type;
     *
     *    constructor(:parameter-name: :type){
     *     this.:variable-name = :parameter-name;
     *    }
     *  }
     *
     *  02)
     *  class :class-name {
     *    constructor(:access-modifier readonly :variable-name: :type){}
     *  }
     * ex)
     *  01)
     *  class Foo {
     *    private readonly MAX_LEN: number = 5;
     *    private readonly MSG: string;
     *
     *    constructor(MSG: string){
     *      this.MSG = MSG;
     *    }
     *  }
     *
     *  02)
     *  class Foo {
     *    private readonly MAX_LEN: number = 5;
     *    constructor(private readonly MSG: string){}
     *  }
     */
    class Department {
        // 01. 일반적인 클래스변수 및 생성자 정의
        // private readonly id: string;
        // public readonly name: string;
        // constructor(id: string, name: string){
        //     this.name = id;
        //     this.name = name;
        // }
        // 02. 생성자 파라미터에 접근 제한자를 사용한 클래스변수 및 생성사 정의    
        constructor(id, name) {
            this.id = id;
            this.name = name;
            this.employees = [];
        }
        // TS에서는 this를 매개변수로 받아 타입을 지정해 줄 수 있다.
        // TS가 이것을 힌트로 파악하여 무엇을 참고해야 할지 알아낼 수 있다.
        //  *철저히 TS의 문법이므로 JS에서는 에러가 발생한다.
        // describe() {
        //     console.log('Department: ' + this.name);
        // }
        describe() {
            console.log(`Department (${this.id}): ${this.name}`);
        }
        addEmployee(employee) {
            this.employees.push(employee);
        }
        printEmployeeInformation() {
            console.log(this.employees.length);
            console.log(this.employees);
        }
    }
    const accounting = new Department('d1', 'Accounting');
    accounting.addEmployee('kim');
    accounting.addEmployee('lee');
    accounting.name = 'class'; //error: 읽기 전용 속성이므로 'name'에 할당할 수 없습니다.
    //class내부의 변수인 employees를 외부에서 변경할 수 있음.
    //클래스를 이용하는 방식을 하나로 정해고 이외는 사용하지 않아야 한다.
    //팀단위로 작업할 경우 팀원마다 다른방식으로 employee를 추가하게 되면 혼선이 빚어진다.
    //따라서, 클래스 외부에서 employees에 접근할 수 없게 해야한다.
    //priavate으로 선언된 eemployees는 에러가 발생한다.
    accounting.employees[2] = 'anna'; //error: 'employees' 속성은 private이며 'Department' 클래스 내에서만 액세스할 수 있습니다.
    accounting.describe();
    // 01. Department클래스의 describe메소드에서 this를 매개변수로 받아 타입을 지정한 경우
    // accountingCopy에 name속성이 없어서 에러처리된다.
    // accountingCopy에 name속성을 추가하면 에러가 없어진다.
    // const accountingCopy = {describe: accounting.describe};
    const accountingCopy = { name: 'CopyAccounting', describe: accounting.describe };
    accountingCopy.describe();
    // 01. Department클래스의 describe메소드에서 this를 매개변수로 받아 타입을 지정하지 않은 경우
    // 콘솔에 undefined가 찍힌다.
    // accountingCopy.describe();
}
