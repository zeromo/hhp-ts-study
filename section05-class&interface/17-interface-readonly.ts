{
/**
 * readonly keyword
 * 인터페이스에서 public이나 private같은 접근제한자는 사용할 수 없지만
 * readonly는 사용가능하다.
 */

interface Greetable {
    // error: 인터페이스 속성에는 이니셜라이저를 사용할 수 없습니다.
    // name: string = 'ppororo';

    // error: 인터페이스에서 public이나 private같은 
    // 접근제한자는 사용할 수 없다
    // public name: string;
    // private age: number;
    // protected prd: string;
    readonly name: string;

    greet(phrase: string): void;
}

class Person implements Greetable {
    name: string;
    age: number = 30;

    constructor(name: string){
        this.name = name;
    }

    greet(phrase: string): void {
        console.log(phrase + '' + this.name);
    }
}

// 변수의 타입에 할당한 인터페이스를 사용해도 문제없다.
const geet: Greetable = new Person('geet');
const max: Person = new Person('MAX');
const none = new Person('nonetype');

// 인터페이스에서 readonly 를 지정해 준후
// 인터페이스타입을 지정한 경우 readonly변수에 setr에러
// 클래스타입을 지정한 경우 readonly변수에 set가능
// 타입을 지정하지 않은 경우 readonly변수에 set가능
geet.name = 'shit';
max.name = 'not max';
none.name = 'none..';
}