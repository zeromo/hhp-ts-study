{

/**
 * ABSTRACT CLASS
 * -추상메소드를 하나라도 갖는 클래스는 abstract키워드를 class키워드 앞에 적용한다.
 * -추상클래스는 인스턴스를 만들 수 없다.
 * -추상클래스인 수퍼클래스를 상속받는 서브클래스는 반드시 추상메서드를 정의해야한다.
 * -이러한 특징으로 서브클래스들의 특성에 따라 다른로직이 수행되지만
 * 동일한 메서드를 가져야 할 경우에 사용한다.
 * 
 * syntax) * 
 * abstract class :class-name {
 *   static :property-name: :type;
 * 
 *   abstract :method-name(): :type;
 * }
 *
 * class :sub-class-name extends :class-name {
 *   //must to define abstract method of super class
 *   :method-name(): :type {};
 * }
 * 
 * ex)
 * 
 */

 abstract class Department {
    static fiscalYear = 2022;

    // private employees: string[] = [];
    // protected: 클래스외부에서 미접근을 유지하며 하위클래스에서 사용하게 해준다.
    protected employees: string[] = [];
    constructor(private readonly id: string, public readonly name: string){}

    static createEmployee(name: string){
        return {name: name};
    }

    abstract describe(this: Department): void;

    addEmployee(employee: string){
        this.employees.push(employee);
    }

    printEmployeeInformation(){
        console.log(this.employees.length);
        console.log(this.employees);
    }
}

new Department(); //error: 추상 클래스의 인스턴스를 만들 수 없습니다.

// 01.
// 하위클래스에 고유한 생성자를 생성하지 않는한
// 상위클래스의 생성자를 사용한다.
// class ITDepartment extends Department {}
// const accounting = new ITDepartment('d1', 'Accounting');

// 02.
// 다른클래스를 상속받는 클래스에서 고유 생성자를 추가하려면
// 반드시 superfmf 추가해 함수처럼 수행해야 합니다.
class ITDepartment extends Department {
    admins: string[];
    
    describe(){
      console.log('this is it');
    }

    constructor(id: string, admins: string[]){
        super(id, 'IT');
        this.admins = admins;
    }
}

class AccountingDepartment extends Department {
    //private 이기 때문에 외부에서 .표기법으로 접근불가함.
    private lastReport: string;

    describe(this: Department): void {
        
    }

    //getter를 통해 외부에서 접근이 가능하게 함.
    //외부에서 사용할때 메소드를 수행하는 것이 아니라
    //입반 프로퍼티에 접근하듯 .표기법을 사용
    get mostRecentReport(){
        if( this.lastReport){
            return this.lastReport;
        }

        throw new Error('No report found...');
    }

    set mostRecentReport(value: string){
        if(!value){
            throw new Error('no send value');
        }
        this.addReport(value);
    }

    constructor(id: string, private reports: string[]){
        super(id, 'ACCOUNTING');
        this.lastReport = reports[0];
    }

    addEmployee(name: string){
        if(name === 'max' ) return;

        // error: 'employees' 속성은 private이며 'Department' 클래스 내에서만 액세스할 수 있습니다.
        // 부모클래스인 Department의 속성인 employees의 접근제한자가 private로 되어있어
        // 해당클래스인 Department에서만 접근이 가능하여 해당 에러가 발생한다.
        // 접근제한자를 protected로 변경하면 클래스외부에서 미접근을 유지하며        
        // 하위클래스에서 사용하게 해준다.
        this.employees.push(name); 
    }

    addReport(text: string){
        this.reports.push(text);
        this.lastReport = text;
    }

    printReports(){
        console.log(this.reports);
    }
}

//static method / static property 접근
const employee1 = Department.createEmployee('ppororo');
Department.fiscalYear;

const it = new ITDepartment('d1', ['Max']);

const account = new AccountingDepartment('d2', []);

account['mostRecentReport'] = 'zzz';
account['mostRecentReport'];

account.addReport('무언가 잘못되었따');
account.printReports();
account.addEmployee('kim');
account.addEmployee('lee');
account.printEmployeeInformation();

}