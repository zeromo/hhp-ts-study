{
/**
 * interface definition for function
 * 인터페이스로 함수의 구조를 정의
 * 
 * 결국, 인터페이스는
 * 클래스 및 함수의 구조를 정의하는데 사용됨
 * 주로 type 키워드를 사용함.
 */

// type AddFn = (a: number, b: number) => number;
interface AddFn {
    (a: number, b: number): number;
}

let add: AddFn;
add = (n1: number, n2: number) => n1 + n2;







interface Named {
    readonly name: string;
}

interface AnotherIF {
    readonly prop: string;
}

// 클래스와 다르게 인터페이스는 여러개를 확장가능
interface Greetable extends Named, AnotherIF {
    greet(phrase: string): void;
}

class Person implements Greetable {
    prop: string = 'another-interface-prop';
    name: string;
    age: number = 30;

    constructor(name: string){
        this.name = name;
    }

    greet(phrase: string): void {
        console.log(phrase + '' + this.name);
    }
}

// 변수의 타입에 할당한 인터페이스를 사용해도 문제없다.
const geet: Greetable = new Person('geet');
const max: Person = new Person('MAX');
const none = new Person('nonetype');

// 인터페이스에서 readonly 를 지정해 준후
// 인터페이스타입을 지정한 경우 readonly변수에 setr에러
// 클래스타입을 지정한 경우 readonly변수에 set가능
// 타입을 지정하지 않은 경우 readonly변수에 set가능
geet.name = 'shit';
max.name = 'not max';
none.name = 'none..';
}