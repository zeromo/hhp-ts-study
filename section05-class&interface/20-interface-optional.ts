{
/**
 * optional
 * ?
 * 선태적 프로퍼티
 */

interface Named {
    readonly name?: string;
    // 해당속성이 있어도 되고 없어도됨
    outputName?: string;

    optionalProperty?: string;
    optionalMethod?(): void;
}

interface AnotherIF {
    readonly prop: string;
}

// 클래스와 다르게 인터페이스는 여러개를 확장가능
interface Greetable extends Named, AnotherIF {
    greet(phrase: string): void;
}

class Person implements Greetable {
    prop: string = 'another-interface-prop';
    name?: string;
    age: number = 30;

    constructor(name?: string){
        if(name)
            this.name = name;
    }

    greet(phrase: string): void {
        if(this.name) {
            console.log(phrase + '' + this.name);
        } else {
            console.log(phrase);
        }
    }
}

const noname: Greetable = new Person();

}