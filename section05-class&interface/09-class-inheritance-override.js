"use strict";
{
    /**
     * 상속-오버라이딩(재정의)
     * -근본 프로퍼티나 메소드는 모든 클래스가 공유하지만
     * 특정 프로퍼티나 메소드를 따로 갖는 클래스가 있을 경우
     * -상위클래스의 메소드를 오버라이드 할 수 있다.
     * -상위클래스의 속성의 접근제한자가 private일 경우
     * 하위클래스에서 접근할 수 없다.
     * private의 특성을 유지하며 하위클래서에서 사용하고 싶을 경우
     * protected를 사용한다.
     */
    class Department {
        constructor(id, name) {
            this.id = id;
            this.name = name;
            // private employees: string[] = [];
            // protected: 클래스외부에서 미접근을 유지하며 하위클래스에서 사용하게 해준다.
            this.employees = [];
        }
        describe() {
            console.log(`Department (${this.id}): ${this.name}`);
        }
        addEmployee(employee) {
            this.employees.push(employee);
        }
        printEmployeeInformation() {
            console.log(this.employees.length);
            console.log(this.employees);
        }
    }
    // 01.
    // 하위클래스에 고유한 생성자를 생성하지 않는한
    // 상위클래스의 생성자를 사용한다.
    // class ITDepartment extends Department {}
    // const accounting = new ITDepartment('d1', 'Accounting');
    // 02.
    // 다른클래스를 상속받는 클래스에서 고유 생성자를 추가하려면
    // 반드시 superfmf 추가해 함수처럼 수행해야 합니다.
    class ITDepartment extends Department {
        constructor(id, admins) {
            super(id, 'IT');
            this.admins = admins;
        }
    }
    class AccountingDepartment extends Department {
        constructor(id, reports) {
            super(id, 'ACCOUNTING');
            this.reports = reports;
        }
        addEmployee(name) {
            if (name === 'max')
                return;
            // error: 'employees' 속성은 private이며 'Department' 클래스 내에서만 액세스할 수 있습니다.
            // 부모클래스인 Department의 속성인 employees의 접근제한자가 private로 되어있어
            // 해당클래스인 Department에서만 접근이 가능하여 해당 에러가 발생한다.
            // 접근제한자를 protected로 변경하면 클래스외부에서 미접근을 유지하며        
            // 하위클래스에서 사용하게 해준다.
            this.employees.push(name);
        }
        addReport(text) {
            this.reports.push(text);
        }
        printReports() {
            console.log(this.reports);
        }
    }
    const it = new ITDepartment('d1', ['Max']);
    const account = new AccountingDepartment('d2', []);
    account.addReport('무언가 잘못되었따');
    account.printReports();
    account.addEmployee('kim');
    account.addEmployee('lee');
    account.printEmployeeInformation();
}
