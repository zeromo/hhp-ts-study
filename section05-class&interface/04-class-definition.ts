{

/**
 * First Class
 * -관례상 이름은 대문자로 시작
 * syntax)
 * class :ClassName {
 *   ...props;
 * 
 *   constructor(...params){
 *     //configuration
 *   }
 * }
 * 
 * ex)
 * class Person {
 *   name: string;
 *   height: number;
 *   age: number;
 *   
 *   constructor(n: string, h: number){
 *     this.name = n;
 *     this.height = h;
 *     this.age = 1;
 *   }
 * }
 * 
 */

// 01. JS 생성자함수
// function Department(n){
//     this.name = n;
//     this.describe = function(){
//         console.log('Department: ' + this.name);
//     }
// }
// Department.prototype.describe = function(){
//     console.log('Department: ' + this.name);
// }

// 02. TS 클래스
class Department {
    // name: string = 'DEFAULT';
    public name: string;

    // 이 클래스 및 이 클래스에 기반하여 생성된 객체에
    // 연결된 함수로, 객체가 생성될 때 실행됨.
    // 객체의 초기화 작업을 수행
    constructor(n: string){
        this.name = n;
    }


    // TS에서는 this를 매개변수로 받아 타입을 지정해 줄 수 있다.
    // TS가 이것을 힌트로 파악하여 무엇을 참고해야 할지 알아낼 수 있다.
    //  *철저히 TS의 문법이므로 JS에서는 에러가 발생한다.
    // describe() {
    //     console.log('Department: ' + this.name);
    // }
    describe(this: Department) {
        console.log('Department: ' + this.name);
    }
}

const accounting = new Department('Accounting');
accounting.describe();

// 01. Department클래스의 describe메소드에서 this를 매개변수로 받아 타입을 지정한 경우
// accountingCopy에 name속성이 없어서 에러처리된다.
// accountingCopy에 name속성을 추가하면 에러가 없어진다.
// const accountingCopy = {describe: accounting.describe};
const accountingCopy = {name: 'CopyAccounting', describe: accounting.describe};
accountingCopy.describe();

// 01. Department클래스의 describe메소드에서 this를 매개변수로 받아 타입을 지정하지 않은 경우
// 콘솔에 undefined가 찍힌다.
// accountingCopy.describe();
}