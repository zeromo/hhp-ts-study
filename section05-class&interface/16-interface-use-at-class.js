"use strict";
{
    class Person {
        constructor(name) {
            this.age = 30;
            this.name = name;
        }
        greet(phrase) {
            console.log(phrase + '' + this.name);
        }
    }
    // 변수의 타입에 할당한 인터페이스를 사용해도 문제없다.
    const geet = new Person('geet');
    const max = new Person('MAX');
}
