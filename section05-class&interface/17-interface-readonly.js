"use strict";
{
    class Person {
        constructor(name) {
            this.age = 30;
            this.name = name;
        }
        greet(phrase) {
            console.log(phrase + '' + this.name);
        }
    }
    // 변수의 타입에 할당한 인터페이스를 사용해도 문제없다.
    const geet = new Person('geet');
    const max = new Person('MAX');
    const none = new Person('nonetype');
    // 인터페이스에서 readonly 를 지정해 준후
    // 인터페이스타입을 지정한 경우 readonly변수에 setr에러
    // 클래스타입을 지정한 경우 readonly변수에 set가능
    // 타입을 지정하지 않은 경우 readonly변수에 set가능
    geet.name = 'shit';
    max.name = 'not max';
    none.name = 'none..';
}
