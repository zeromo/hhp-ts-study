"use strict";
{
    /**
     * 상속
     * 근본 프로퍼티나 메소드는 모든 클래스가 공유하지만
     * 특정 프로퍼티나 메소드를 따로 갖는 클래스가 있을 경우
     */
    class Department {
        constructor(id, name) {
            this.id = id;
            this.name = name;
            this.employees = [];
        }
        describe() {
            console.log(`Department (${this.id}): ${this.name}`);
        }
        addEmployee(employee) {
            this.employees.push(employee);
        }
        printEmployeeInformation() {
            console.log(this.employees.length);
            console.log(this.employees);
        }
    }
    // 01.
    // 하위클래스에 고유한 생성자를 생성하지 않는한
    // 상위클래스의 생성자를 사용한다.
    // class ITDepartment extends Department {}
    // const accounting = new ITDepartment('d1', 'Accounting');
    // 02.
    // 다른클래스를 상속받는 클래스에서 고유 생성자를 추가하려면
    // 반드시 superfmf 추가해 함수처럼 수행해야 합니다.
    class ITDepartment extends Department {
        constructor(id, admins) {
            super(id, 'IT');
            this.admins = admins;
        }
    }
    class AccountingDepartment extends Department {
        constructor(id, reports) {
            super(id, 'ACCOUNTING');
            this.reports = reports;
        }
        addReport(text) {
            this.reports.push(text);
        }
        printReports() {
            console.log(this.reports);
        }
    }
    const it = new ITDepartment('d1', ['Max']);
    const account = new AccountingDepartment('d2', []);
    account.addReport('무언가 잘못되었따');
    account.printReports();
}
