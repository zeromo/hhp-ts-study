{
// https://www.typescriptlang.org/docs/handbook/utility-types.html
// 01. Partial
// 객체의 모든 속성을 optional로 처리한다.
interface CourseGoal {
    title: string,
    description: string,
    completeUntil: Date
}

function createCourseGoal(
    title: string,
    description: string,
    date: Date
): CourseGoal {
    // let courseGoal: CourseGoal = {} ;
    let courseGoal: Partial<CourseGoal> = {} ;
    courseGoal.title =  title;
    courseGoal.description =  description;
    courseGoal.completeUntil =  date;

    return courseGoal as CourseGoal;
}

// 02. Readonly
// 객체자체를 읽기전용 속성으로 처리한다.
const names: Readonly<string[]> = ['ppp', 'aaa'];
// names.push('mamam');
// names.pop();



}