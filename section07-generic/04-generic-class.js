"use strict";
{
    /**
     * 01. generic class
    */
    class DataStorage {
        constructor() {
            this.data = [];
        }
        addItem(item) {
            this.data.push(item);
        }
        removeITem(item) {
            this.data.splice(this.data.indexOf(item), 1);
        }
        getItems() {
            return [...this.data];
        }
    }
    class DataStorageG {
        constructor() {
            this.data = [];
        }
        addItem(item) {
            this.data.push(item);
        }
        removeITem(item) {
            this.data.splice(this.data.indexOf(item), 1);
        }
        getItems() {
            return [...this.data];
        }
    }
    const textStorage = new DataStorageG();
    const numberStorage = new DataStorageG();
    class DataStorageT {
        constructor() {
            this.data = [];
        }
        addItem(item) {
            this.data.push(item);
        }
        removeITem(item) {
            this.data.splice(this.data.indexOf(item), 1);
        }
        getItems() {
            return [...this.data];
        }
    }
    const textStorageT = new DataStorageT();
    const numberStorageT = new DataStorageT();
    const objStorageT = new DataStorageT();
    // JS CODE
    // var __spreadArray = (this && this.__spreadArray) || funcvar __spreadArray = (this && this.__spreadArray) || function (to, from, pack) {
    //     if (pack || arguments.length === 2) for (var i = 0, l = from.length, ar; i < l; i++) {
    //         if (ar || !(i in from)) {
    //             if (!ar) ar = Array.prototype.slice.call(from, 0, i);
    //             ar[i] = from[i];
    //         }
    //     }
    //     return to.concat(ar || Array.prototype.slice.call(from));
    // };
    // var DataStorageG = /** @class */ (function () {
    //     function DataStorageG() {
    //         this.data = [];
    //     }
    //     DataStorageG.prototype.addItem = function (item) {
    //         this.data.push(item);
    //     };
    //     DataStorageG.prototype.removeITem = function (item) {
    //         this.data.splice(this.data.indexOf(item), 1);
    //     };
    //     DataStorageG.prototype.getItems = function () {
    //         return __spreadArray([], this.data, true);
    //     };
    //     return DataStorageG;
    // }());
    // var textStorage = new DataStorageG();
    // var numberStorage = new DataStorageG();
}
