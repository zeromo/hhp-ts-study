"use strict";
{
    /**
     * built-in generic / generic
     */
    // 01. Array Generic
    // type Array = string[];
    const notypename = ['a', 'b'];
    const name = ['a', 'b'];
    const nameG = ['a', 'b'];
    notypename[0].split('');
    name[0].split('');
    nameG[0].split('');
    // 02. Promise Generic
    // const promise: Promise<unknown>
    const promise = new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve('This is done!');
        }, 1000);
    });
    // const promiseG: Promise<string>
    const promiseG = new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve('This is done!');
        }, 1000);
    });
}
