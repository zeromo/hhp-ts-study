"use strict";
/**
 * 01. typescript 모듈설치
 * npm install typescript -g
 *
 * 02. package init
 * npm init
 *
 * 03. lite-server 모듈설치
 * npm install lite-server --save-dev
 *  *
 * 04. script속성에 값 추가
 * "start": "lite-server"
 *
 * 05. ts파일 compile 명령어: tsc :filename.ts *
 * tsc app.ts
 *
 * 06. 서버실행 명령어
 * npm start
 *
 * etc. vscode extensions...
 * -ex lint
 * -path intellisense
 * -prettier
 * ...
 */ 
